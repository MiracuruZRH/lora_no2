void setup() {
    while(!Serial)
        ; // wait for Serial to be initialized
    Serial.begin(115200);
    delay(100); // per sample code on inlora module test
    Serial.println(F("Starting"));

#ifdef VCC_ENABLE
    // For Pinoccio Scout boards
    pinMode(VCC_ENABLE, OUTPUT);
    digitalWrite(VCC_ENABLE, HIGH);
    delay(1000);
#endif

    os_init();    // LMIC init
    LMIC_reset(); // Reset the MAC state. Session and pending data transfers
                  // will be discarded.
    // Let LMIC compensate for +/- 1% clock error
    LMIC_setClockError(MAX_CLOCK_ERROR * 1 / 100);
    // Set static session parameters. Instead of dynamically establishing a
    // session by joining the network, precomputed session parameters are be
    // provided.
#ifdef PROGMEM
    // On AVR, these values are stored in flash and only copied to RAM
    // once. Copy them to a temporary buffer here, LMIC_setSession will
    // copy them into a buffer of its own again.
    uint8_t appskey[sizeof(APPSKEY)];
    uint8_t nwkskey[sizeof(NWKSKEY)];
    memcpy_P(appskey, APPSKEY, sizeof(APPSKEY));
    memcpy_P(nwkskey, NWKSKEY, sizeof(NWKSKEY));
    LMIC_setSession(0x13, DEVADDR, nwkskey, appskey);
#else
    // If not running an AVR with PROGMEM, just use the arrays directly
    LMIC_setSession(0x13, DEVADDR, NWKSKEY, APPSKEY);
#endif

#if defined(CFG_in866) // three channels are set for india as per ttn guidelines
    LMIC_setupChannel(0, 865062500, DR_RANGE_MAP(DR_SF7, DR_SF12),
                      BAND_CENTI); // g-band
    LMIC_setupChannel(1, 865402500, DR_RANGE_MAP(DR_SF7, DR_SF12),
                      BAND_CENTI); // g-band
    LMIC_setupChannel(2, 865985000, DR_RANGE_MAP(DR_SF7, DR_SF12),
                      BAND_CENTI); // g-band
    LMIC_setupChannel(3, 866550000, DR_RANGE_MAP(DR_SF10, DR_SF10),
                      BAND_CENTI); // g-band
#endif

    LMIC_setLinkCheckMode(0);    // Disable link check validation
    LMIC.dn2Dr = DR_SF10;        // TTN uses SF10 for its RX2 window in India
    LMIC_setDrTxpow(DR_SF7, 14); // Set data rate and transmit power for uplink
    digitalWrite(8, HIGH);
    do_send(&sendjob); // Start the job and send packets
    // delay(500);
    digitalWrite(8, LOW);
}