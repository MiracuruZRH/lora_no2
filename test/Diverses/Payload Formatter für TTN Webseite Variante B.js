function decodeUplink(input) {
    return {
        data: {
            bytes: input.bytes[1] / 10
        },
        warnings: [],
        errors: []
    };
}