function decodeUplink(input) {
    var data = {};
    var events = {
        1: "humidity",
        2: "temperature"
    };
    data.event = events[input.fPort];
    data.temperature = (((input.bytes[0] << 8) + input.bytes[1]) / 10) + " °C";
    data.humidity = (((input.bytes[2] << 8) + input.bytes[3]) / 100) + "%";
    return {
        data: data,
        events: events,
        warnings: [],
        errors: []
    };
}

// Payload unformatiert sieht so aus (Testdaten):
// 00 2E 1D AF
// 00 2E in DEC =    46
// 1D AF in DEC = 7'599

// Ergebnis mit der obenstehenden Funktion
// Temp.  4.6 °C
// Hum.  75.99%

// Ergebnis. 
/*
"received_at": "2021-12-11T23:46:17.021159618Z",
"uplink_message": {
  "session_key_id": "AX2r47IKNB6/gcW9buEjmg==",
  "f_port": 1,
  "frm_payload": "AC0=",
  "decoded_payload": {
    "event": "humidity",
    "humidity": "NaN%",
    "temperature": "4.5 °C"
  }
*/